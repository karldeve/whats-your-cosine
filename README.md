# What's Your Co(Sine)?

## Description
This is the source code for a simple browser game, "What's Your (Co)Sine?", available for free at https://sacrificialprawn.itch.io/whats-your-cosine. This is a puzzle game of sorts that involves identifying which frequency from the DCT for a greyscale grid has been removed to create a second greyscale grid. It is built using vanilla JS and the HTML canvas.

## Usage
The whatsyourcosine.html file contains the full game, so it can be played by simply loading this file in a browser.

## Support
If you find a bug or want to suggest an improvement (or collaboration), feel free to raise an issue (no promises).

## License
This project is licensed under MPL2.
